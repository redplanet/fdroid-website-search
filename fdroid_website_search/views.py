# from haystack.views import SearchView
from haystack.generic_views import SearchView

from .forms import FDroidSearchForm
from .models import Language

from .templatetags import fdroid_website_search_tags

class FDroidSearchView(SearchView):

    form_class = FDroidSearchForm

    def get_context_data(self, **kwargs):
        context = super(FDroidSearchView, self).get_context_data(**kwargs)

        lang = fdroid_website_search_tags._map_lang(self.request.GET.get('lang', ''))
        if lang:
            try:
                Language.objects.get(name=lang)
                context['lang'] = lang
            except Language.DoesNotExist:
                pass

        return context
